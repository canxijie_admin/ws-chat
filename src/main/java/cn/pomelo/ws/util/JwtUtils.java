package cn.pomelo.ws.util;

import cn.pomelo.ws.domain.ex.ServerException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
public class JwtUtils {

    private static final long EXPIRE_TIME = 30 * 60 * 1000; // 30分钟
    private static final String SECRET = "HSyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9";

    /**
     * 生成签名
     *
     * @param user 要加密的字符串
     * @return 加密的token
     */
    public static String sign(String user) {
        return sign(user, SECRET);
    }

    /**
     * 生成签名
     *
     * @param user   要加密的字符串
     * @param secret 密钥
     * @return 加密的token
     */
    public static String sign(String user, String secret) {
        if (secret == null) {
            secret = SECRET;
        }

        return JWT.create().withSubject(user)
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRE_TIME)) // 设置过期时间
                .sign(Algorithm.HMAC256(secret)); // 设置签名算法
    }

    /**
     * 校验token是否正确
     *
     * @param token 要校验的token
     * @return 是否正确
     */
    public static boolean verify(String token) {
        return verify(token, SECRET);
    }

    /**
     * 校验token是否正确
     *
     * @param token  要校验的token
     * @param secret 密钥
     * @return 是否正确
     */
    public static boolean verify(String token, String secret) {
        try {
            JWT.require(Algorithm.HMAC256(secret)).build().verify(token);
            return true;
        } catch (Exception e) {
            log.error("token 校验失败", e);
            return false;
        }
    }

    /**
     * 获得token中的信息 无需secret解密也能获得
     *
     * @param token 要解析的token
     * @return token中包含的
     */
    public static String getSubject(String token) {
        if (token == null) {
            throw new ServerException("请先登录", 401);
        }
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getSubject();
        } catch (JWTDecodeException e) {
            throw new ServerException("token解析错误或token过期", 400);
        }

    }

    /**
     * 获得token中的信息无需secret解密也能获得
     *
     * @param token 要解析的token
     * @return token中包含的用户名
     */
    public static String getUser(String token) {
        DecodedJWT jwt = JWT.decode(token);
        return jwt.getClaim("username").asString();
    }
}

