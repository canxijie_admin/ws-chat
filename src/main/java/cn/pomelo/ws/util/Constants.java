package cn.pomelo.ws.util;

public class Constants {

    public static final String PRODUCT_IMAGE_PRE = "/images/";

    public static final String REDIS_LOGIN = "loginUser";
}
