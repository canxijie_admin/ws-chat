package cn.pomelo.ws.mapper;

import cn.pomelo.ws.domain.entity.Friendship;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FriendshipMapper {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Friendship record);

    Friendship selectByPrimaryKey(Integer id);

    List<Friendship> selectByUId(int uid);
}