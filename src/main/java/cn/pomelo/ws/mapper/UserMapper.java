package cn.pomelo.ws.mapper;

import cn.pomelo.ws.domain.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(User record);

    /**
     * 按照id查询用户 只封装用户的基本信息
     * @param id 用户id
     * @return 用户的基本信息
     */
    User selectByPrimaryKey(Integer id);

    /**
     * 按照id查询用户 封装用户的好友关系
     * @param id 用户id
     * @return 用户的基本信息以及好友信息
     */
    User selectById(Integer id);

    User selectByUsernameAndPassword(String username, String password);
}