package cn.pomelo.ws.mapper;

import cn.pomelo.ws.domain.entity.Message;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MessageMapper {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(Message record);

    Message selectByPrimaryKey(Integer id);

    List<Message> selectBySendAndReceive(Integer sid, Integer rid);

    Message selectLastBySendAndReceive(Integer sid, Integer rid);
}