package cn.pomelo.ws.config;

import cn.pomelo.ws.domain.entity.User;
import cn.pomelo.ws.util.Constants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class SpringConfig implements WebMvcConfigurer {

    @Value("${file.path}")
    private String location;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 配置静态资源映射
        registry.addResourceHandler(Constants.PRODUCT_IMAGE_PRE + "**").addResourceLocations("file:" + location);
    }

    @Bean(name = "listRedisTemplate")
    public RedisTemplate<String, List<Integer>> listRedisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, List<Integer>> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new JdkSerializationRedisSerializer());
        return template;
    }
}
