package cn.pomelo.ws.config;

import cn.hutool.json.JSONUtil;
import cn.pomelo.ws.domain.entity.User;
import cn.pomelo.ws.domain.ex.ServerException;
import cn.pomelo.ws.util.JwtUtils;
import org.springframework.http.HttpStatus;

import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;
import java.util.List;

/**
 * 在 WebSocket 建立握手时，服务器会调用 modifyHandshake 方法，可以在握手的过程中进行一些自定义操作。需要再 ServerEndpoint 中声明
 */
public class GetHttpSessionConfigurator extends ServerEndpointConfig.Configurator {
    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        System.out.println(request.getParameterMap().get("token"));
        // 由于 WebSocket 不能直接携带请求头信息，因此使用 拼接 url 的方式进行拼接，从请求参数中获取 token 信息
        List<String> token = request.getParameterMap().get("token");
        if (token == null || token.isEmpty()) {
            // 如果不存在抛出一个异常，表示用户未登录，返回 HTTP 401 未授权状态码。
            throw new ServerException("请先登录", HttpStatus.UNAUTHORIZED.value());
        }
        // 存储登录的用户信息，这样在握手成功后可以通过 Session.getUserProperties().get("user") 获取登录的用户信息
        sec.getUserProperties().put("user", JSONUtil.toBean(JwtUtils.getSubject(token.get(0)), User.class));
    }
}
