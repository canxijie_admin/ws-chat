package cn.pomelo.ws.domain.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 浏览器发送给客户端的 Message 对象
 */
@Data
public class SysMessage implements Serializable {

    private String toName;
    private String message;

}
