package cn.pomelo.ws.domain.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 服务端发送给客户端的响应内容实体
 */
@Data
public class ResultMessage implements Serializable {

    private Object message;
    private String fromName;
    private boolean isSystem;
}
