package cn.pomelo.ws.domain.ex;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServerException extends RuntimeException{

    private int code;
    public ServerException(String msg, int code) {
        super(msg);
        this.code = code;
    }
}