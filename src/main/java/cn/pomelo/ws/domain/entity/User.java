package cn.pomelo.ws.domain.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class User implements Serializable {
    private Integer id;

    private String username;

    private String password;

    private String avatar;

    private Date createTime;

    private List<Friendship> friendships;
}