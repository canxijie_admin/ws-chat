package cn.pomelo.ws.domain.entity;

import java.util.Date;

import lombok.Data;

@Data
public class Friendship {
    private Integer id;

    private Integer userId;

    private User user;

    private Integer friendId;

    private User friend;

    private String status;

    private Date createTime;

    private Message last; // 最近一次的聊天信息
}