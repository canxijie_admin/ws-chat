package cn.pomelo.ws.domain.entity;

import java.util.Date;
import lombok.Data;

@Data
public class Message {
    private Integer id;

    private Integer senderId;

    private Integer receiverId;

    private String content;

    private Date createTime;
}