package cn.pomelo.ws.handler;

import cn.pomelo.ws.domain.ex.ServerException;
import cn.pomelo.ws.domain.resp.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理
 */
@RestControllerAdvice
@Slf4j
public class GlobalHandler {

    /**
     * 处理 ServerException 异常
     * @param e 异常
     */
    @ExceptionHandler(ServerException.class)
    public Result handlerServerException(ServerException e) {
        log.error(e.getMessage());
        return Result.failure(e.getMessage(), e.getCode());
    }
}

