package cn.pomelo.ws.service;

import cn.pomelo.ws.domain.entity.Friendship;

import java.util.List;

public interface FriendshipService{

    int deleteByPrimaryKey(Integer id);

    int insertSelective(Friendship record);

    Friendship selectByPrimaryKey(Integer id);

    List<Friendship> selectByUid(int i);
}
