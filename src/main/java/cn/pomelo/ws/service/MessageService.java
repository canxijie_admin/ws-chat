package cn.pomelo.ws.service;

import cn.pomelo.ws.domain.entity.Message;

import java.util.List;

public interface MessageService{

    int deleteByPrimaryKey(Integer id);

    int insertSelective(Message record);

    Message selectByPrimaryKey(Integer id);

    List<Message> selectBySendAndReceive(Integer sid, Integer rid);

}
