package cn.pomelo.ws.service.impl;

import cn.pomelo.ws.domain.entity.Message;
import cn.pomelo.ws.mapper.MessageMapper;
import cn.pomelo.ws.service.MessageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {

    @Resource
    private MessageMapper messageMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return messageMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(Message record) {
        return messageMapper.insertSelective(record);
    }

    @Override
    public Message selectByPrimaryKey(Integer id) {
        return messageMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Message> selectBySendAndReceive(Integer sid, Integer rid) {
        return messageMapper.selectBySendAndReceive(sid, rid);
    }

}
