package cn.pomelo.ws.service.impl;

import cn.pomelo.ws.domain.entity.Friendship;
import cn.pomelo.ws.mapper.FriendshipMapper;
import cn.pomelo.ws.service.FriendshipService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FriendshipServiceImpl implements FriendshipService{

    @Resource
    private FriendshipMapper friendshipMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return friendshipMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(Friendship record) {
        return friendshipMapper.insertSelective(record);
    }

    @Override
    public Friendship selectByPrimaryKey(Integer id) {
        return friendshipMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Friendship> selectByUid(int id) {
        return null;
    }

}
