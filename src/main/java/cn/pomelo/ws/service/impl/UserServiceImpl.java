package cn.pomelo.ws.service.impl;

import cn.hutool.crypto.SecureUtil;
import cn.pomelo.ws.domain.entity.User;
import cn.pomelo.ws.mapper.UserMapper;
import cn.pomelo.ws.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService{

    @Resource
    private UserMapper userMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return userMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(User record) {
        return userMapper.insertSelective(record);
    }

    @Override
    public User selectByPrimaryKey(Integer id) {
        return userMapper.selectById(id);
    }

    @Override
    public User login(String username, String password) {
        return userMapper.selectByUsernameAndPassword(username, SecureUtil.md5(password));
    }

}
