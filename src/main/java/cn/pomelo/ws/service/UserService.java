package cn.pomelo.ws.service;

import cn.pomelo.ws.domain.entity.User;
public interface UserService{

    int deleteByPrimaryKey(Integer id);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    User login(String username, String password);

}
